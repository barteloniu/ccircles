#ifndef WASM

#include <math.h>

#else

#include "wasm_stubs.h"

#endif

#include "vector.h"

Vector mul(Vector vector, double scalar) {
    return (Vector){vector.x * scalar, vector.y * scalar, vector.z * scalar};
}

Vector add(Vector v, Vector u) {
    return (Vector){v.x + u.x, v.y + u.y, v.z + u.z};
}

Vector sub(Vector v, Vector u) {
    return (Vector){v.x - u.x, v.y - u.y, v.z - u.z};
}

double norm(Vector v) {
    double sx = v.x * v.x;
    double sy = v.y * v.y;
    double sz = v.z * v.z;
    return sqrt(sx + sy + sz);
}

double dot(Vector v, Vector u) {
    return v.x * u.x + v.y * u.y + v.z * u.z;
}

Vector normalise(Vector vector) {
    return mul(vector, 1.0 / norm(vector));
}

#ifndef VECTOR_H
#define VECTOR_H

typedef struct {
    double x;
    double y;
    double z;
} Vector;

Vector mul(Vector vector, double scalar);
Vector add(Vector v, Vector u);
Vector sub(Vector v, Vector u);
double norm(Vector v);
double dot(Vector v, Vector u);
Vector normalise(Vector vector);

#endif

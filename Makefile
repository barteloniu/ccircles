CC := gcc
CFLAGS := -O2 -Wall -Wextra -lm

objects := ccircles.o writer.o vector.o utils.o

all: ccircles.png

ccircles.png: ccircles.pam
	magick $< $@

ccircles.pam: ccircles
	./ccircles

ccircles: $(objects)
	$(CC) $(CFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm ccircles $(objects) ccircles.png ccircles.pam

.SUFFIXES:
.PHONY: all clean

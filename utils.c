#include "utils.h"

double min(double a, double b) {
    return (a < b) ? a : b;
}

double max(double a, double b) {
    return (a > b) ? a : b;
}

double map(double value, double in_min, double in_max, double out_min,
           double out_max) {
    return (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

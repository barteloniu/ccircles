#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>

typedef struct {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
} Color;

double min(double a, double b);
double max(double a, double b);
double map(double value, double in_min, double in_max, double out_min,
           double out_max);

#endif

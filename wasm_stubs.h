#ifndef WASM_STUBS_H
#define WASM_STUBS_H

#define HUGE_VAL 999999.0

double fmod(double, double);
double sin(double);
double cos(double);
double sqrt(double);

#endif

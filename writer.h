#ifndef WRITER_H
#define WRITER_H

#include <stdint.h>
#include <stdio.h>
#include "utils.h"

FILE *create_file(const char *filename, int width, int height);
void write_canvas(FILE *file, Color *canvas, int width, int height);

#endif

const ctx = document.querySelector("#canvas").getContext("2d")

const worker = new Worker("worker.js")

worker.onmessage = event => {
    ctx.putImageData(event.data, 0, 0)
}

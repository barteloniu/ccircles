#ifndef WASM

#include <math.h>
#include <stdlib.h>
#include "writer.h"

#else

#include "wasm_stubs.h"

#endif

#include <stdbool.h>
#include <stdint.h>
#include "utils.h"
#include "vector.h"

#define FILENAME "ccircles.pam"
#define WIDTH 640
#define HEIGHT 480

#define M_PI_2 1.57079632679489661923
#define FOV M_PI_2

const Vector LIGHT_VECTOR = {-0.5, -1, -0.7};
const Vector SPHERE_CENTER = {1, 1, 1};
#define SPHERE_RADIUS 0.5
#define MOD 2.0

#define EPSILON 0.01
#define MAX_RAY_LENGTH 20

// helper function for calculating the distance
Vector modded(Vector v) {
    v.x = fmod(v.x, MOD);
    v.x += (v.x < 0) ? MOD : 0;
    v.y = fmod(v.y, MOD);
    v.y += (v.y < 0) ? MOD : 0;
    v.z = fmod(v.z, MOD);
    v.z += (v.z < 0) ? MOD : 0;
    return v;
}

// ---------
// the following two functions should be the only things one has to modify
// to change the rendered object, the rest should work as it is
double distance(Vector v) {
    return norm(sub(modded(v), SPHERE_CENTER)) - SPHERE_RADIUS;
}

double light_cos(Vector v) {
    Vector normal = normalise(sub(modded(v), SPHERE_CENTER));
    return dot(normalise(LIGHT_VECTOR), normal);
}
// ---------

void render(Color *canvas, Vector starting_point, double angle) {
    for (int y = 0; y < HEIGHT; y++) {
        for (int x = 0; x < WIDTH; x++) {
            canvas[x + y * WIDTH] = (Color){0, 0, 0, 255};

            double horizontal_deg = map(x, 0, WIDTH, -FOV / 2 + angle, FOV / 2 + angle);
            double vertical_deg = map(y, 0, HEIGHT, -FOV / 2 * HEIGHT / WIDTH,
                                      FOV / 2 * HEIGHT / WIDTH);

            Vector dir_vec
                = {sin(horizontal_deg) * cos(vertical_deg), sin(vertical_deg),
                   cos(horizontal_deg) * cos(vertical_deg)};

            Vector vec = starting_point;
            double closest = HUGE_VAL;
            while (norm(sub(vec, starting_point)) < MAX_RAY_LENGTH) {
                double dist = distance(vec);
                closest = min(dist, closest);

                if (dist < EPSILON) {
                    canvas[x + y * WIDTH].b = 255;

                    double cosine = light_cos(vec);

                    canvas[x + y * WIDTH].r
                        = map(max(cosine, 0), 0.0, 1.0, 0, 255);

                    break;
                }

                vec = add(vec, mul(dir_vec, dist));
            }
        }
    }
}

#ifndef WASM
// native target entrypoint

int main(void) {
    FILE *file = create_file(FILENAME, WIDTH, HEIGHT);

    Color canvas[WIDTH * HEIGHT] = {0};
    Vector camera = {0, 0, 0};
    render(canvas, camera, 0);

    write_canvas(file, canvas, WIDTH, HEIGHT);
    fclose(file);
}

#else
// when compiling to wasm the module is more of a library, so no main function
// to spare myself from writing a memory allocator
// i just declared a bunch of globals that do the job fine

Color global_canvas[640][480];
Vector global_camera = {0, 0, 0};
const Vector STEP = {0.1, 0, 0.1};

void go_forward() {
    global_camera = add(global_camera, STEP);
}

#endif

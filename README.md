# ccircles

## A simple CPU-based raymarcher written in plain C.

See it live [here](https://barteloniu.gitlab.io/ccircles/).

To run locally just clone and `make`.

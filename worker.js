const WIDTH = 640;
const HEIGHT = 480;

const importObject = {
    env: {
        fmod: (x, y) => x % y,
        sin: Math.sin,
        cos: Math.cos,
        sqrt: Math.sqrt,
    }
}

let w;
WebAssembly.instantiateStreaming(fetch("ccircles.wasm"), importObject).then(
    obj => {
        w = obj.instance
        
        requestAnimationFrame(frame)
    }
)

const frame = () => {
    w.exports.render(w.exports.global_canvas, w.exports.global_camera, Math.PI / 4)
    w.exports.go_forward()

    const array = new Uint8ClampedArray(w.exports.memory.buffer, w.exports.global_canvas, WIDTH * HEIGHT * 4)
    const imageData = new ImageData(array, WIDTH, HEIGHT)
    postMessage(imageData)

    requestAnimationFrame(frame)
}

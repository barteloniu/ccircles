#include "writer.h"

FILE *create_file(const char *filename, int width, int height) {
    FILE *file = fopen(filename, "wb");
    fprintf(file,
            "P7\n"
            "WIDTH %d\n"
            "HEIGHT %d\n"
            "DEPTH 4\n"
            "MAXVAL 255\n"
            "TUPLTYPE RGB_ALPHA\n"
            "ENDHDR\n",
            width, height);
    return file;
}

void write_color(FILE *file, Color color) {
    fwrite(&color, sizeof(Color), 1, file);
}

void write_canvas(FILE *file, Color *canvas, int width, int height) {
    fwrite(canvas, sizeof(Color), width * height, file);
}
